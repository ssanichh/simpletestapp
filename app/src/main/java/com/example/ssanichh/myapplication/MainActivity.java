package com.example.ssanichh.myapplication;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

class SimpleaMap extends HashMap<String,String> {
    public static final String NAME="datetime";
    public SimpleaMap(String datetime){
        super();
        super.put(NAME,datetime);
    }
}

public class MainActivity extends Activity {
    private RelativeLayout layoutBackground;
    private Random rnd = new Random();
    private TextView currentDateTime;
    private DatabaseHelper mHelper;
    private SQLiteDatabase mDatabase;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layoutBackground=(RelativeLayout)findViewById(R.id.relativeLayout);
        currentDateTime=(TextView)findViewById(R.id.currentDateTime);
        mHelper=new DatabaseHelper(this, "dateTimeDataBase.db", null, 1);
        mDatabase=mHelper.getWritableDatabase();
        listView=(ListView)findViewById(R.id.listView);
    }

    @Override
    public void onResume(){
        super.onResume();
        ArrayList<SimpleaMap> list=getDataFromDB();
        ListAdapter adapter=new SimpleAdapter(getApplicationContext(),list,
                R.layout.list_item,
                new String[]{SimpleaMap.NAME},
                new int[]{R.id.dateTimeItem});
        listView.setAdapter(adapter);
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        mDatabase.close();
    }

    private ArrayList<SimpleaMap> getDataFromDB(){
        Cursor userCursor=mDatabase.query("dateTimeTable",
                new String[]{DatabaseHelper.DATETIME_COLUMN},
                null,null,null,null,null);
        userCursor.moveToFirst();
        ArrayList<SimpleaMap> list= new ArrayList<>();
        while(!userCursor.isLast()){
            list.add(new SimpleaMap
                    (userCursor.getString
                            (userCursor.getColumnIndex(DatabaseHelper.DATETIME_COLUMN))));
            userCursor.moveToNext();
        }
        userCursor.moveToLast();
        list.add(new SimpleaMap
                (userCursor.getString
                        (userCursor.getColumnIndex(DatabaseHelper.DATETIME_COLUMN))));
        userCursor.close();
        return list;
    }

    private void updateContent(String currentDT){
        currentDateTime.setText(currentDT);

        ArrayList<SimpleaMap> list=getDataFromDB();
        ListAdapter adapter=new SimpleAdapter(getApplicationContext(),list,
                R.layout.list_item,
                new String[]{SimpleaMap.NAME},
                new int[]{R.id.dateTimeItem});
        listView.setAdapter(adapter);

        ContentValues newValues=new ContentValues();
        newValues.put(DatabaseHelper.DATETIME_COLUMN,currentDT);
        mDatabase.insert("dateTimeTable", null, newValues);
    }

    public void onChangeColorClick(View view) {
        layoutBackground.setBackgroundColor(Color.rgb(rnd.nextInt(255),
                rnd.nextInt(255), rnd.nextInt(255)));
    }

    public void onGetDataClick(View view) {
        if(!hasConnection()){
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Для этой операции необходимо подключение к интернету.",
                    Toast.LENGTH_LONG);
            toast.show();
        }else {
            new DataGetter().execute();
        }
    }

    private String convertDate(String timestamp){
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String formattedDate = dateFormat.format(new Date(Long.valueOf(timestamp)*1000));
        return formattedDate;
    }

    private boolean hasConnection(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }else{
            return false;
        }
    }

    class DataGetter extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... path) {
            HttpURLConnection urlConnection;
            String response="response";

            try {
                URL url = new URL("http://android-logs.uran.in.ua/test.php");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setReadTimeout(10000);
                urlConnection.connect();

                BufferedReader reader= new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line;
                while((line=reader.readLine())!=null){
                    response+=line;
                }
            } catch (IOException e) {
                Toast toast = Toast.makeText(getApplicationContext(),e.getMessage(),
                        Toast.LENGTH_LONG);
                toast.show();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            String[] values=response.split(" ");
            String result="";
            for(int i=0;i<values.length-2; i++){
                if(values[i].equals("[REQUEST_TIME]")&&values[i+1].equals("=>")){
                    result=values[i+2];
                }
            }
            if(result.contains(")")){
                result=result.substring(0,result.length()-1);
            }

            if(!result.equals("")){
                String formattedResult = convertDate(result);
                updateContent(formattedResult);
            }
        }
    }
}
